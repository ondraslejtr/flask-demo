from flask import Flask
from flask import render_template
from flask import request

from db import db_session
from models import User

# Instance of class that will be our WSGI application
app = Flask(__name__)


@app.route('/')
def index():
    return 'Welcome to our Flask page!'


@app.route('/hello')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('index.html', name=name)


################ USERS ####################

# Autogenerates HEAD and OPTIONS
@app.route('/users/', methods=['GET', 'POST'])
def users():
    user_list = ''.join([f'<li>{user}</li>' for user in get_users()])
    return 'We are currently storing these users:<ul>' + str(user_list) + '</ul>'


@app.route('/users/<username>', methods=['GET', 'POST'])
def user_profile(username):
    if request.method == 'POST':
        add_user(username)
        return 'User added'
    else:
        if user_exists(username):
            return f'User {username} exists'
        else:
            return f'User {username} doesn\'t exist'


################## DATABASE ##################


def get_users():
    return User.query.all()


def add_user(username):
    db_session.add(User(username))
    db_session.commit()


def user_exists(username):
    if User.query.filter(User.name == username).first() is not None:
        return True
    return False


# Teardown connection to DB
@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


if __name__ == '__main__':
    app.run(debug=True, port=8000)
