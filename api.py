from flask import Flask
from flask_restful import reqparse, abort, Api, Resource


app = Flask(__name__)
api = Api(app)


# "Database" placeholder
TODOS = {
    1: {'task': 'build Flask API'},
    2: {'task': '?????'},
    3: {'task': 'profit!'},
}


def abort_if_todo_doesnt_exist(todo_id):
    if todo_id not in TODOS:
        abort(404, message="Todo {} doesn't exist".format(todo_id))


# Flask parser that will validate and parse incoming request body into dict
parser = reqparse.RequestParser()
parser.add_argument('task')


# TodoList resource definition
# shows a list of all todos, and lets you POST to add new tasks
class TodoListResource(Resource):
    def get(self):
        return TODOS

    def post(self):
        args = parser.parse_args()
        # Simulate primary key growth
        todo_id = int(max(TODOS.keys())) + 1
        TODOS[todo_id] = {'task': args['task']}
        return {todo_id: TODOS[todo_id]}, 201

# Todo
# shows a single todo item and lets you delete a todo item
class TodoResource(Resource):
    def get(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        return TODOS[todo_id]

    def delete(self, todo_id):
        abort_if_todo_doesnt_exist(todo_id)
        del TODOS[todo_id]
        return '', 204

    def put(self, todo_id):
        args = parser.parse_args()
        task = {'task': args['task']}
        TODOS[todo_id] = task
        return task, 201


# Register resource for discovery
# Register multiple URLs for same resource
api.add_resource(TodoListResource, '/todos')
api.add_resource(TodoResource, '/todos/<int:todo_id>')

if __name__ == '__main__':
    app.run(debug=True, port=8001)
